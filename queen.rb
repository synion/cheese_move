require_relative 'movement.rb'
class Queen
  include Movement

  def initialize(position)
    @position = position
  end

  def to_s
    move(queen_vector)
  end
end
