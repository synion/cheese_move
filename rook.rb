require_relative 'movement.rb'
class Rook
  include Movement

  def initialize(position)
    @position = position
  end

  def to_s
    move(horizon_and_vertical_vector)
  end
end
