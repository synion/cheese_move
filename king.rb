require_relative 'movement.rb'
class King
  include Movement

  def initialize(position)
    @position = position
  end

  def to_s
    move(king_vector)
  end

end
