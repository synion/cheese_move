require_relative 'movement.rb'

class Bishop
  include Movement

  def initialize(position)
    @position = position
  end

  def to_s
    move(digonal_vector)
  end

end

