require_relative '../rook.rb'
describe Rook do
  context 'new with args' do
    it "be a Rook" do
      rook = Rook.new('b2')
      expect(rook).to be_a_kind_of Rook
    end
  end
  context 'see posible move' do
    it "give all posible move" do
      rook = Rook.new('b4')
      expect(rook.to_s).to eq('a4 b1 b2 b3 b5 b6 b7 b8 c4 d4 e4 f4 g4 h4')
    end
  end
  context 'access to private method' do
    it 'have no access' do
      rook = Rook.new('b4')
      expect {rook.coordinates}.to raise_error(NoMethodError)
    end
  end
  context 'access to private method' do
    it 'have no access' do
      rook = Rook.new('b4')
      expect {rook.vectors(horizon_and_vertical_vector)}.
      to raise_error(NameError)
    end
  end
  context 'access to private method' do
    it 'have no access' do
      rook = Rook.new('b4')
      expect {rook.move(horizon_and_vertical_vector)}.
      to raise_error(NameError)
    end
  end
end
