require_relative '../king.rb'
describe King do
  context 'new with args' do
    it "be a King" do
      king = King.new('b2')
      expect(king).to be_a_kind_of King
    end
  end
  context 'see posible move' do
    it "give all posible move" do
      king = King.new('b4')
      expect(king.to_s).to eq('a3 a4 a5 b3 b5 c3 c4 c5')
    end
  end

end
