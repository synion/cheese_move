require_relative '../queen.rb'
describe Queen do
  context 'new with args' do
    it "be a Queen" do
      queen = Queen.new('b2')
      expect(queen).to be_a_kind_of Queen
    end
  end
  context 'see posible move' do
    it "give all posible move" do
      queen = Queen.new('b4')
      expect(queen.to_s).
      to eq('a3 a4 a5 b1 b2 b3 b5 b6 b7 b8 c3 c4 c5 d2 d4 d6 e1 e4 e7 f4 f8 g4 h4')
    end
  end
  context 'access to private method' do
    it 'have no access' do
      queen = Queen.new('b4')
      expect {queen.coordinates}.to raise_error(NoMethodError)
    end
  end
  context 'access to private method' do
    it 'have no access' do
      queen = Queen.new('b4')
      expect {queen.queen_vector(queen_vector)}.to raise_error(NameError)
    end
  end
  context 'access to private method' do
    it 'have no access' do
      queen = Queen.new('b4')
      expect {queen.move(queen_vector)}.to raise_error(NameError)
    end
  end
end
