require_relative '../bishop.rb'
describe Bishop do
  context 'new with args' do
    it "be a Bishop" do
      bishop = Bishop.new('b2')
      expect(bishop).to be_a_kind_of Bishop
    end
  end
  context 'see posible move' do
    it "give all posible move" do
      bishop = Bishop.new('b4')
      expect(bishop.to_s).to eq('a3 a5 c3 c5 d2 d6 e1 e7 f8')
    end
  end
  context 'access to private method' do
    it 'have no access' do
      bishop = Bishop.new('b4')
      expect {bishop.coordinates}.to raise_error(NoMethodError)
    end
  end
  context 'access to private method' do
    it 'have no access' do
      bishop = Bishop.new('b4')
      expect {bishop.vectors(digonal_vector)}.to raise_error(NameError)
    end
  end
  context 'access to private method' do
    it 'have no access' do
      bishop = Bishop.new('b4')
      expect {bishop.move(digonal_vector)}.to raise_error(NameError)
    end
  end
end
