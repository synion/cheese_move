require_relative '../knight.rb'
describe Knight do
  context 'new with args' do
    it "be a Knight" do
      knight = Knight.new('b2')
      expect(knight).to be_a_kind_of Knight
    end
  end
  context 'see posible move' do
    it "give all posible move" do
      knight = Knight.new('b4')
      expect(knight.to_s).to eq('a2 a6 c2 c6 d3 d5')
    end
  end
end
