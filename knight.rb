require_relative 'movement.rb'
class Knight
  include Movement

  def initialize(position)
    @position = position
  end

  def to_s
    move(knight_vector)
  end
end

