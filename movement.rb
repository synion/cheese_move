module Movement
  VECTORS_HV = ((-7..-1).to_a + (1..7).to_a).
              map {|numb| [[0,numb],[numb,0]]}.flatten(1)
  VECTORS_DIGONAL =  1.upto(7).flat_map { |numb| [numb, -numb].repeated_permutation(2).to_a }
  VECTORS_KNIGHT = [
              [2, -1], [2, 1], [-2, -1], [-2, 1],
              [1, -2], [1, 2], [-1, -2], [-1, 2]
            ]
  VECTORS_KING = [*-1..1].repeated_permutation(2).to_a - [[0,0]]
  private
  class Coordinates
    LETTER = "_abcdefgh"
    NUMBER = "_12345678"
    attr_accessor :x,:y

    def initialize(x,y)
      @x = x
      @y = y
    end

    def self.new_from_position(position)
      self.new(LETTER.index(position[0]), NUMBER.index(position[1]))
    end

    def valid?
      (1..8).include?(x) &&
      (1..8).include?(y)
    end

    def to_s
      self.valid? ? "#{x_position}#{y_position}" : nil
    end

    def x_position
      LETTER[x]
    end

    def y_position
      NUMBER[y]
    end

    def +(coordinates)
      self.class.new(x + coordinates.x, y + coordinates.y)
    end
  end

  def coordinates
    @coordinate ||= Coordinates.new_from_position(@position)
  end

  def vectors(vector_type)
    @vectors ||= vector_type.map { |cord_x,cord_y| Coordinates.new(cord_x,cord_y)}
  end

  def move(vector_type)
    vectors(vector_type).
      map {|vector| (vector + coordinates).to_s }.
      compact.sort.join(' ')
  end

  def horizon_and_vertical_vector
    VECTORS_HV
  end

  def digonal_vector
    VECTORS_DIGONAL
  end

  def knight_vector
    VECTORS_KNIGHT
  end

  def queen_vector
    horizon_and_vertical_vector + digonal_vector
  end

  def king_vector
    VECTORS_KING
  end
end
